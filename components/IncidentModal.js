import React from "react";
import PropTypes from "prop-types";
import { Modal, Button } from "antd";
import { CheckCircleFilled, InfoCircleFilled, QuestionCircleFilled, CoffeeOutlined, ApiOutlined } from "@ant-design/icons";

function IncidentModal(props) {
    console.log(props.comments)
    return (
        <Modal title={props.title} visible={props.visible} onCancel={() => props.updateVisible(false)}
            footer={[
                <Button onClick={() => props.updateVisible(false)}>
                Закрыть
                </Button>,
          ]}>
            <h2 className="incident-title">{props.description}</h2>
            <h2 className="incident-status" style={{color: props.color}}>
                {props.icon.render()}&nbsp;
                {props.status}
            </h2>
            {props.comments && props.comments.length > 0 ? <div className="table">
                    <div className="table-row">
                        {Object.entries(props.comments).map((comment) => {
                            comment = comment[1]
                            switch(comment.status) {
                                case 0: return (<h2 className="incident-status"><span className="istatus-err"><QuestionCircleFilled /> Обнаружен</span> - {comment.message} ({comment.time})</h2>); break;
                                case 1: return(<h2 className="incident-status"><span className="istatus-warn"><InfoCircleFilled /> Тестируется</span> - {comment.message} ({comment.time})</h2>); break;
                                case 2: return(<h2 className="incident-status"><span className="istatus-ok"><CheckCircleFilled /> Завершено</span> - {comment.message} ({comment.time})</h2>); break;
                            }
                        })}
                    </div>
                </div> : ''}
        </Modal>
    )
}

IncidentModal.propTypes = {
    id: PropTypes.number,
    title: PropTypes.string,
    date: PropTypes.string,
    status: PropTypes.string,
    icon: PropTypes.object,
    color: PropTypes.string,
    description: PropTypes.string,
    comments: PropTypes.array,
    visible: PropTypes.bool,
    updateVisible: PropTypes.func,
};

export { IncidentModal };