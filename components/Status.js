import React from "react";
import PropTypes from "prop-types";

export const Status = (props) => {
    return(
        <>
            {props.type === "online" &&
                <div className="status-display status-ok">
                    Все системы работают в штатном режиме
                </div>
            }
            {props.type === "partiallyonline" &&
                <div className="status-display status-warn">
                    Некоторые сервисы или услуги могут работать некорректно
                </div>
            }
            {props.type === "outage" &&
                <div className="status-display status-outage">
                    Системы недоступны в следствии форс-мажорной ситуации
                </div>
            }
        </>
    );
};

Status.propTypes = {
    type: PropTypes.string
};
