import React from "react";
import PropTypes from "prop-types";
import { IncidentModal } from "./IncidentModal";
import { CheckCircleFilled, InfoCircleFilled, QuestionCircleFilled, CoffeeOutlined, ApiOutlined } from "@ant-design/icons";
import Router from "next/router";

export const Incident = (props) => {
    const [modal, setModalVisible] = React.useState(false);
    let status, icon, color;
    switch(props.status) {
        case 0: {
            status = "Обнаружен"; color = "#ff4444"; icon = QuestionCircleFilled;
            break;
        } case 1: {
            status = "Тестируется"; color = "#ff9100"; icon = InfoCircleFilled;
            break;
        } case 2: {
            status = "Завершено"; color = "#00c853"; icon = CheckCircleFilled;
            break;
        } case 3: {
            status = "Запланировано"; color = "#00c853"; icon = CoffeeOutlined;
            break;
        } case 4: {
            status = "Проводятся работы"; color = "#FFA900"; icon = ApiOutlined;
            break;
        }
    }
    return(
        <div className={!props.title ? "table-row" : "table-row active"}>
            {!props.title &&
                <>
                    <h2>{props.date}</h2>
                    <h2>Инциденты не найдены</h2>
                </>
            }
            {props.title &&
                <>
                <IncidentModal id={props.id} title={props.title} date={props.date} status={status} icon={icon} color={color} description={props.description} comments={props.comments} visible={modal} updateVisible={setModalVisible} />
                <span onClick={props.title ? () => setModalVisible(true) : null}>
                    <h2>{props.date}</h2>
                    <h2 className="table-row-title">{props.title}</h2>
                    <h3 className="table-row-status">
                        <span style={{color: color}}>
                            {icon.render()}
                            &nbsp;{status}</span> - {props.description}
                        <br/>
                        Пострадали системы: {props.affected ? props.affected : "Нет"}
                    </h3>
                </span>
                </>
            }
        </div>
    );
};

Incident.propTypes = {
    id: PropTypes.number,
    title: PropTypes.string,
    date: PropTypes.string,
    status: PropTypes.number,
    description: PropTypes.string,
    affected: PropTypes.string,
    comments: PropTypes.array
};