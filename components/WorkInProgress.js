import React from "react";
import PropTypes from "prop-types";
import { FieldTimeOutlined, CodeOutlined, ApiOutlined } from "@ant-design/icons";

export const WorkInProgress = (props) => {
    return(
        <div className="status-wip status-warn">
            <div className="wip-row">Запланированы технические работы {props.date1}</div>
            <div className="wip-row"><CodeOutlined /> {props.description}</div>
            <div className="wip-row"><ApiOutlined /> Затронуты сервисы: {props.services}</div>
            <div className="wip-row"><FieldTimeOutlined /> Время работ: {props.date}</div>
        </div>
    );
};
 
WorkInProgress.propTypes = {
    date: PropTypes.string,
    description: PropTypes.string,
    services: PropTypes.string
};