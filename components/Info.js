import React from "react";
import PropTypes from "prop-types";

export const Info = (props) => {
    return(
        <div className="status-display status-info">
            Информация: {props.text}
            {props.back ? <a onClick={() => window.history.back()}>Назад</a> : <></>}
        </div>
    );
};

Info.propTypes = {
    text: PropTypes.string,
    back: PropTypes.bool
};