var Incidents = require('./models/incidents');
var moment = require('moment')
export default async function handler(req, res) {
    const maintenances = await Incidents.find({ type: "maintenance" }).exec()
    const response = [];
    new Promise((resolve, reject) => {
        if(maintenances.length == 0) resolve();
        maintenances.forEach((maintenance, index, array) => {
            if(moment().diff(maintenance.maintenance.dateFrom, 'days') >= -3 && moment(maintenance.maintenance.dateTo) > moment()) {
                response.push(maintenance)
            }
            if(index == array.length-1) resolve();
        })
    }).then(() => {
        res.status(200).json(response);
    })
}