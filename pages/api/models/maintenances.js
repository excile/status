var mongoose = require('./db')
const { Schema } = mongoose;
const Maintenances = new Schema({ 
    date: String,
    title: String,
    description: String,
    affected: Array,
    comments: [{ status: Number, message: String, time: String }],
    type: String,
 });

module.exports = mongoose.models.maintenances || mongoose.model('maintenances', Maintenances);