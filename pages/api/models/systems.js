var mongoose = require('./db');
const { Schema } = mongoose;
const Systems = new Schema({ 
    name: String,
    status: Number,
    source: String,
 });

module.exports = mongoose.models.systems || mongoose.model('systems', Systems);