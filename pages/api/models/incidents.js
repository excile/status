var mongoose = require('./db');
const { Schema } = mongoose;
const Incidents = new Schema({ 
    date: String,
    title: String,
    description: String,
    affected: Array,
    comments: [{ status: Number, message: String, time: String }],
    maintenance: {
        status: Number,
        dateFrom: String,
        dateTo: String
    },
    id: String,
    type: String,
 });

module.exports = mongoose.models.incidents || mongoose.model('incidents', Incidents);