var Incidents = require('./models/incidents');
var moment = require('moment')
export default async function handler(req, res) {
    const data = [];
    for(let i = 0; i < 7; i++) {
        data.push({
            date: moment().subtract(i, 'd').format('MM/DD/YYYY'),
            incidents: await Incidents.find({ date: moment().subtract(i, 'd').format('MM/DD/YYYY') }).exec()
        })
    }
    res.status(200).json(data);
}