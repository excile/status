var Systems = require('./models/systems');
var moment = require('moment')
export default async function handler(req, res) {
    res.setHeader('Access-Control-Allow-Credentials', true)
    res.setHeader('Access-Control-Allow-Origin', '*')
    let count = await Systems.count({ status: 1 });
    let status = 0;
    if(count == 0) status = 0;
    else if(count == 1) status = 1;
    else status = 2;
    res.status(200).json({ status: status });
}