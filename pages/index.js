import React from "react";
import { Button, Skeleton } from "antd";
import { LoadingOutlined } from '@ant-design/icons';
import { Status } from "../components/Status";
import { WorkInProgress } from "../components/WorkInProgress";
import { Info } from "../components/Info";
import { Incident } from "../components/Incident";
import axios from "axios"
import 'moment/locale/ru'  // without this line it didn't work
import moment from "moment"; // бот гей
moment.locale('ru')
const index = () => {
    const [incidents, setIncidents] = React.useState(false);
    const [maintenances, setMaintenances] = React.useState([]);
    const [information, setInformation] = React.useState(false);
    React.useEffect(() => {
        axios.get(`/api/incidents`).then(({data}) => {
            setIncidents(data)
        }).catch(err => console.log(err))
        axios.get(`/api/maintenances`).then(({data}) => {
            setMaintenances(data)
        }).catch(err => console.log(err))
        axios.get(`/api/information`).then(({data}) => {
            setInformation(data)
        }).catch(err => console.log(err))
    }, [])
    return(
        <>
            <head>
                <title>Berch Cloud</title>
                <meta name="description" content="Статус сервисов Berch.Cloud" />
                <link rel="icon" href="/favicon.png" />
            </head>
            <body>
                <div className="status-header">
                    <img className="md-logo" src="favicon.png" alt={"Berch.Cloud"} onClick={() => window.location.href = "https://berch.cloud"} />
                    <div className="align-right-flex">
                        <Button type="primary" shape="round" onClick={() => window.location.href = "https://my.berch.cloud"}>
                      Биллинг
                        </Button>
                    </div>
                </div>
                <div style={{margin: '0 auto', paddingTop: '5vh', width: '80%'}}>
                    {information && information.status == 0 ? <Status type="online" /> : information.status == 1 ? <Status type="partiallyonline" /> : <Status type="outage" />}
                    {Object.entries(maintenances).map((maintenance) => {
                        maintenance = maintenance[1];
                        return(<WorkInProgress date1={moment(maintenance.date).format('LL')} date={`${moment(maintenance.maintenance.dateFrom).format('lll')} - ${moment(maintenance.maintenance.dateTo).format('lll')}`} description={maintenance.title} services={maintenance.affected.join(', ')} />)
                    })}
                </div>
                <h1>Последние инциденты</h1>        
                <div className="table" style={{marginBottom: '15px'}}>
                    {incidents ? <>
                        {Object.entries(incidents).map((incident) => {
                            incident = incident[1];
                            console.log(incident)
                            if(incident.incidents.length == 0) return (<Incident id={0} date={moment(incident.date).format('ll')} />)
                            if(incident.incidents[0].comments.length > 0) incident.status = incident.incidents[0].comments[incident.incidents[0].comments.length-1].status;
                            else if(incident.incidents[0].type == 'maintenance' && moment(incident.incidents[0].maintenance.dateFrom) > moment()) incident.status = 3;
                            else incident.status = 4;
                            return (<Incident id={incident.incidents[0].id} date={moment(incident.incidents[0].date).format('ll')} title={incident.incidents[0].title} status={incident.status} description={incident.incidents[0].description} affected={incident.incidents[0].affected.join(', ')} comments={incident.incidents[0].comments} />)
                        })}
                    </> : <Skeleton active style={{marginRight: '20px'}} />}
                </div>
                <p style={{color: '#ccc', marginTop: '10px', textAlign: 'center'}}>Разработано <a href="https://github.com/excile1">excile!</a></p>
            </body>
        </>
    );
};

export default index;